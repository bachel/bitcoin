<div class="row">
  <div class="col-sm-12">
    <div class="well">
      <h1 class="text-center">Basic Daten</h1>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-sm-6">
    <div class="well">
      <h2 class="text-center">Basic</h2>
      <h3 class="text-center">FaucetApi: <?php echo $FaucetBoxApi; ?> </h3>
      <h3 class="text-center">FaucetMin: <?php echo $FaucetBoxMin; ?> </h3>
      <h3 class="text-center">ThreasholdIncome: <?php echo $ThreasholdIncome; ?> </h3>
      <h3 class="text-center">ThreasholdSplitter: <?php echo $ThreasholdSplitter; ?> </h3>
      <h3 class="text-center">ThreasholdAdSplitter: <?php echo $ThreasholdAdSplitter; ?> </h3>
      <h3 class="text-center">BtcFee: <?php echo $BtcFee; ?> </h3>
      <h3 class="text-center">ImportTaxCanada: <?php echo $ImportTaxCanada; ?> </h3>
      <h3 class="text-center">ImportTaxDeutschland: <?php echo $ImportTaxDeutschland; ?> </h3>
      <h3 class="text-center">HomeMiners: <?php echo $HomeMiners; ?> </h3>
      <h3 class="text-center">ScryptHomeMiners: <?php echo $ScryptHomeMiners; ?> </h3>
    </div>
  </div>
  <div class="col-sm-6">
    <div class="well">
      <h2 class="text-center">Basic bearbeiten</h2>
      <form  action="/?page=basic" method="post">
          <div class="form-group">
            <label for="FaucetApi">FaucetApi</label>
            <input type="text" class="form-control" id="FaucetApi" name="FaucetBoxApi" value="<?php echo $FaucetBoxApi; ?>">
          </div>
          <div class="form-group">
            <label for="FaucetMin">FaucetMin</label>
            <input type="text" class="form-control" id="FaucetMin" name="FaucetBoxMin" value="<?php echo $FaucetBoxMin; ?>">
          </div>
          <div class="form-group">
            <label for="ThreasholdIncome">ThreasholdIncome</label>
            <input type="text" class="form-control" id="ThreasholdIncome" name="ThreasholdIncome" value="<?php echo $ThreasholdIncome; ?>">
          </div>
          <div class="form-group">
            <label for="ThreasholdSplitter">ThreasholdSplitter</label>
            <input type="text" class="form-control" id="ThreasholdSplitter" name="ThreasholdSplitter" value="<?php echo $ThreasholdSplitter; ?>">
          </div>
          <div class="form-group">
            <label for="ThreasholdAdsplitt">ThreasholdAdsplitt</label>
            <input type="text" class="form-control" id="ThreasholdAdsplitt" name="ThreasholdAdsplitt" value="<?php echo $ThreasholdAdSplitter; ?>">
          </div>
          <div class="form-group">
            <label for="BtcFee">BtcFee</label>
            <input type="text" class="form-control" id="BtcFee" name="BtcFee" value="<?php echo $BtcFee; ?>">
          </div>
          <div class="form-group">
            <label for="ImportCa">ImportCa</label>
            <input type="text" class="form-control" id="ImportCa" name="ImportCa" value="<?php echo $ImportTaxCanada; ?>">
          </div>
          <div class="form-group">
            <label for="ImportDe">ImportDe</label>
            <input type="text" class="form-control" id="ImportDe" name="ImportDe" value="<?php echo $ImportTaxDeutschland; ?>">
          </div>
          <div class="form-group">
            <label for="HomeMiners">HomeMiners</label>
            <input type="text" class="form-control" id="HomeMiners" name="HomeMiners" value="<?php echo $HomeMiners; ?>">
          </div>
          <div class="form-group">
            <label for="ScryptHomeMiners">ScryptHomeMiners</label>
            <input type="text" class="form-control" id="ScryptHomeMiners" name="ScryptHomeMiners" value="<?php echo $ScryptHomeMiners; ?>">
          </div>
          <button type="submit" class="btn btn-default" name="basicupdate">Submit</button>
      </form>
    </div>
  </div>
</div>
