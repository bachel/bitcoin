<div class="row">
  <div class="col-sm-12">
    <div class="well">
      <h1 class="text-center">Genesis Mining</h1>
      <h3 class="text-center"> Genesis Hashrate:
        <?php
        echo $GenesisMiningHashpowerDisplay;
        ?>
      </h3>
      <h3 class="text-center"> Genesis Income per Day:
        <?php
        echo $GenesisMiningIncome - $GenesisMiningPowerCost.' $';
        ?>
      </h3>
      <h3 class="text-center"> Genesis Income per Month:
        <?php
        echo (($GenesisMiningIncome - $GenesisMiningPowerCost) * 30).' $';
        ?>
      </h3>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-sm-6">
    <div class="well">
      <h1 class="text-center">Genesis</h1><br>
          <table class="table">
            <thead>
            <tr>
            <th>Art</th>
            <th>Genesis Miner Kosten</th>
            <th>HashingPower in Ths</th>
            <th>Energiekosten per Ghs in Usd</th>
            </tr>
            </thead>
            <tbody>
              <tr>
                <td>BTC Miner</td>
                <td><?php echo $MinerPrice3.' $'; ?></td>
                <td><?php echo $ApwPrice3.' Th/s'; ?></td>
                <td><?php echo $MinerKw3.' $'; ?></td>
              </tr>
            </tbody>
          </table>
    </div>
    </div>

    <div class="col-sm-6">
      <div class="well">
        <h1 class="text-center">Genesis bearbeiten</h1><br>
        <form  action="/?page=genesis" method="post">
            <div class="form-group">
              <label for="GenesiMinerKosten">GenesiMinerKosten</label>
              <input type="text" class="form-control" id="GenesiMinerKosten" name="GenesiMinerKosten" value="<?php echo $MinerPrice3; ?>">
            </div>
            <div class="form-group">
              <label for="GenesisThs">GenesisThs</label>
              <input type="text" class="form-control" id="GenesisThs" name="GenesisThs" value="<?php echo $ApwPrice3; ?>">
            </div>
            <div class="form-group">
              <label for="CostPerGhs">CostPerGhs</label>
              <input type="text" class="form-control" id="CostPerGhs" name="CostPerGhs" value="<?php echo $MinerKw3; ?>">
            </div>
            <button type="submit" class="btn btn-default" name="updateGenisis">Submit</button>
        </form>
      </div>
      </div>
</div>
