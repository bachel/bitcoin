<div class="row">
  <div class="col-sm-12">
    <div class="well">
      <h1 class="text-center">Miner</h1>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-sm-6">
    <div class="well">
      <h1 class="text-center">Miner</h1><br>
          <table class="table">
            <thead>
            <tr>
            <th>Art</th>
            <th>Miner Kosten</th>
            <th>Netzteil Kosten</th>
            <th>Versand Kosten</th>
            <th>Energieverbrauch in KW</th>
            </tr>
            </thead>
            <tbody>
              <tr>
                <td>BTC Miner</td>
                <td><?php echo $MinerPrice.' $'; ?></td>
                <td><?php echo $ApwPrice.' $'; ?></td>
                <td><?php echo $ShippingCost.' $'; ?></td>
                <td><?php echo $MinerKw; ?></td>
              </tr>
              <tr>
                <td>Scrypt Miner</td>
                <td><?php echo $MinerPrice2.' $'; ?></td>
                <td><?php echo $ApwPrice2.' $'; ?></td>
                <td><?php echo $ShippingCost2.' $'; ?></td>
                <td><?php echo $MinerKw2; ?></td>
              </tr>
            </tbody>
          </table>
    </div>
    </div>

    <div class="col-sm-6">
      <div class="well">
        <h1 class="text-center">Miner bearbeiten</h1><br>
        <form  action="/?page=miner" method="post">
            <div class="form-group">
              <label for="BtcMinerKosten">BtcMinerKosten</label>
              <input type="text" class="form-control" id="BtcMinerKosten" name="BtcMinerKosten" value="<?php echo $MinerPrice; ?>">
            </div>
            <div class="form-group">
              <label for="BtcNetzteilKosten">BtcNetzteilKosten</label>
              <input type="text" class="form-control" id="BtcNetzteilKosten" name="BtcNetzteilKosten" value="<?php echo $ApwPrice; ?>">
            </div>
            <div class="form-group">
              <label for="BtcVersandKosten">BtcVersandKosten</label>
              <input type="text" class="form-control" id="BtcVersandKosten" name="BtcVersandKosten" value="<?php echo $ShippingCost; ?>">
            </div>
            <div class="form-group">
              <label for="BtcMinerKw">BtcMinerKw</label>
              <input type="text" class="form-control" id="BtcMinerKw" name="BtcMinerKw" value="<?php echo $MinerKw; ?>">
            </div>
            <div class="form-group">
              <label for="LtcMinerKosten">LtcMinerKosten</label>
              <input type="text" class="form-control" id="LtcMinerKosten" name="LtcMinerKosten" value="<?php echo $MinerPrice2; ?>">
            </div>
            <div class="form-group">
              <label for="LtcNetzteilKosten">LtcNetzteilKosten</label>
              <input type="text" class="form-control" id="LtcNetzteilKosten" name="LtcNetzteilKosten" value="<?php echo $ApwPrice2; ?>">
            </div>
            <div class="form-group">
              <label for="LtcVersandKosten">LtcVersandKosten</label>
              <input type="text" class="form-control" id="LtcVersandKosten" name="LtcVersandKosten" value="<?php echo $ShippingCost2; ?>">
            </div>
            <div class="form-group">
              <label for="LtcMinerKw">LtcMinerKw</label>
              <input type="text" class="form-control" id="LtcMinerKw" name="LtcMinerKw" value="<?php echo $MinerKw2; ?>">
            </div>
            <button type="submit" class="btn btn-default" name="updateMiner">Submit</button>
        </form>
      </div>
      </div>
</div>
