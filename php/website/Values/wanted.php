<div class="row">
  <div class="col-sm-12">
    <div class="well">
      <h1 class="text-center">Wanted</h1>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-sm-6">
    <div class="well">
      <h1 class="text-center">Wanted</h1><br>
          <table class="table">
            <thead>
            <tr>
            <th>Was</th>
            <th>Menge</th>
            </tr>
            </thead>
            <tbody>
              <tr>
                <td>BtcMiner</td>
                <td><?php echo $RentMinerWanted; ?></td>
              </tr>
              <tr>
                <td>LtcMiner</td>
                <td><?php echo $HomeMinerWanted; ?></td>
              </tr>
              <tr>
                <td>BtcMiner On Sale</td>
                <td><?php echo $MinerOnSale; ?></td>
              </tr>
              <tr>
                <td>LtcMiner On Sale</td>
                <td><?php echo $ScryptMinerOnSale; ?></td>
              </tr>
            </tbody>
          </table>
    </div>
    </div>

    <div class="col-sm-6">
      <div class="well">
        <h1 class="text-center">Wanted bearbeiten</h1><br>
        <form  action="/?page=wanted" method="post">
            <div class="form-group">
              <label for="BtcMiner">BtcMiner</label>
              <input type="text" class="form-control" id="BtcMiner" name="BtcMiner" value="<?php echo $RentMinerWanted; ?>">
            </div>
            <div class="form-group">
              <label for="LtcMiner">LtcMiner</label>
              <input type="text" class="form-control" id="LtcMiner" name="LtcMiner" value="<?php echo $HomeMinerWanted; ?>">
            </div>
            <div class="form-group">
              <label for="BtcMinerOnSale">BtcMinerOnSale</label>
              <input type="text" class="form-control" id="BtcMinerOnSale" name="BtcMinerOnSale" value="<?php echo $MinerOnSale; ?>">
            </div>
            <div class="form-group">
              <label for="LtcMinerOnSale">LtcMinerOnSale</label>
              <input type="text" class="form-control" id="LtcMinerOnSale" name="LtcMinerOnSale" value="<?php echo $ScryptMinerOnSale; ?>">
            </div>
            <button type="submit" class="btn btn-default" name="updateWanted">Submit</button>
        </form>
      </div>
      </div>
</div>
