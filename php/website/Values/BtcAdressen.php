<div class="row">
  <div class="col-sm-12">
    <div class="well">
      <h1 class="text-center">Wallet Adressen</h1>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-sm-6">
    <div class="well">
      <h1 class="text-center">Adressen</h1><br>
          <table class="table">
            <thead>
            <tr>
            <th>Account</th>
            <th>Adresse</th>
            </tr>
            </thead>
            <tbody>
              <tr>
                <td>Income</td>
                <td><?php echo $IncomeAccountAdress; ?></td>
              </tr>
              <tr>
                <td>Splitter</td>
                <td><?php echo $SplittingAccountAdress; ?></td>
              </tr>
              <tr>
                <td>Invest</td>
                <td><?php echo $InvestAccountAdress; ?></td>
              </tr>
              <tr>
                <td>Energy</td>
                <td><?php echo $EnergyAccountAdress; ?></td>
              </tr>
              <tr>
                <td>Faucet</td>
                <td><?php echo $FaucetBoxAdress; ?></td>
              </tr>
              <tr>
                <td>Bitcoin.de</td>
                <td><?php echo $BitcoinDeAdress; ?></td>
              </tr>
              <tr>
                <td>My Paying Crypto Ads</td>
                <td><?php echo $MycryptoadsAdress; ?></td>
              </tr>
              <tr>
                <td>My Paying  Ads</td>
                <td><?php echo $MypayingadsAdress; ?></td>
              </tr>
              <tr>
                <td> Ads Splitter</td>
                <td><?php echo $AdsplitAdress; ?></td>
              </tr>
            </tbody>
          </table>
    </div>
    </div>

    <div class="col-sm-6">
      <div class="well">
        <h1 class="text-center">Adressen bearbeiten</h1><br>
        <form  action="/?page=adress" method="post">
            <div class="form-group">
              <label for="Income">Income</label>
              <input type="text" class="form-control" id="Income" name="income" value="<?php echo $IncomeAccountAdress; ?>">
            </div>
            <div class="form-group">
              <label for="Splitter">Splitter</label>
              <input type="text" class="form-control" id="Splitter" name="splitter" value="<?php echo $SplittingAccountAdress; ?>">
            </div>
            <div class="form-group">
              <label for="Invest">Invest</label>
              <input type="text" class="form-control" id="Invest" name="invest" value="<?php echo $InvestAccountAdress; ?>">
            </div>
            <div class="form-group">
              <label for="Energy">Energy</label>
              <input type="text" class="form-control" id="Energy" name="energy" value="<?php echo $EnergyAccountAdress; ?>">
            </div>
            <div class="form-group">
              <label for="Faucet">Faucet</label>
              <input type="text" class="form-control" id="Faucet" name="faucetbox" value="<?php echo $FaucetBoxAdress; ?>">
            </div>
            <div class="form-group">
              <label for="Bitcoin.de">Bitcoin.de</label>
              <input type="text" class="form-control" id="Faucet" name="bitcoinde" value="<?php echo $BitcoinDeAdress; ?>">
            </div>
            <div class="form-group">
              <label for="MyPayingCryptoAds">My Paying Crypto Ads</label>
              <input type="text" class="form-control" id="MyPayingCryptoAds" name="cryptoads" value="<?php echo $MycryptoadsAdress; ?>">
            </div>
            <div class="form-group">
              <label for="MyPayingAds">My Paying Ads</label>
              <input type="text" class="form-control" id="MyPayingAds" name="Mypayngadsadress" value="<?php echo $MypayingadsAdress; ?>">
            </div>
            <div class="form-group">
              <label for="AdsSplitter">Ads Splitter</label>
              <input type="text" class="form-control" id="AdsSplitter" name="adsplittadress" value="<?php echo $AdsplitAdress; ?>">
            </div>
            <button type="submit" class="btn btn-default" name="updateAdress">Submit</button>
        </form>




      </div>
      </div>
</div>
