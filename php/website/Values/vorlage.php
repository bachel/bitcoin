<div class="row">
  <div class="col-sm-12">
    <div class="well">
      <h1 class="text-center">Wallet Adressen</h1>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-sm-6">
    <div class="well">
      <h1 class="text-center">Adressen</h1><br>
          <table class="table">
            <thead>
            <tr>
            <th>Account</th>
            <th>Adresse</th>
            </tr>
            </thead>
            <tbody>
              <tr>
                <td>Income</td>
                <td><?php echo $IncomeAccountAdress; ?></td>
              </tr>
            </tbody>
          </table>
    </div>
    </div>

    <div class="col-sm-6">
      <div class="well">
        <h1 class="text-center">Adressen bearbeiten</h1><br>
        <form  action="/?page=adress" method="post">
            <div class="form-group">
              <label for="Income">Income</label>
              <input type="text" class="form-control" id="Income" name="income" value="<?php echo $IncomeAccountAdress; ?>">
            </div>
            <button type="submit" class="btn btn-default" name="updateAdress">Submit</button>
        </form>
      </div>
      </div>
</div>
