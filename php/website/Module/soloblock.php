<?php
include '../MasterWebsite.php';
 ?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="refresh" content="300">
    <meta http-equiv="cache-control" content="max-age=0" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="expires" content="0" />
    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="pragma" content="no-cache" />
    <meta name="viewport" content="width=device-width">
    <meta name="google" content="notranslate">
    <script src="https://use.fontawesome.com/4c508f666e.js"></script>
    <script src="https://use.typekit.net/ctv0mfx.js"></script>
    <script>try{Typekit.load({ async: true });}catch(e){}</script>
    <link rel="stylesheet" href="../../bower_components/bootstrap/dist/css/bootstrap.min.css" >
    <link rel="stylesheet" href="../../css/main.css" >
    <script src="../../bower_components/jquery/dist/jquery.min.js" charset="utf-8"></script>
    <script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js" charset="utf-8"></script>
    <title>Bitcoin Manager Daten Bearbeiten</title>
  </head>
  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <a class="navbar-brand" href="/">Bitcoin Manager</a>
        </div>
        <ul class="nav navbar-nav">
          <li><a href="/">Home</a></li>
          <li><a href="WalletSend.php">Wallet</a></li>
          <li><a href="ChangeData.php">Change Values</a></li>
          <li><a href="soloblock.php">Solo Block Info</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right" >
          <li ><a><strong>1</strong> <span class="glyphicon glyphicon-xbt"></span> <strong>=</strong></a></li>
          <li><a><strong ><?php echo  $EUR_BitcoinPrice;?></strong> <span class="glyphicon glyphicon-euro"></span> </a></li>
          <li><a><strong><?php echo $USD_BitcoinPrice; ?></strong> <span class="glyphicon glyphicon-usd"></span> </a></li>
        </ul>
      </div>
    </nav>


    <main class="container-fluid">
  <div class="row">
    <div class="col-sm-12">
      <div class="well">
        <h1 class="text-center"> Solo Mining Stats</h1>
        <h4 class="text-center">Hashrate:<?php echo $OneMinuteHash; ?></h4>
      </div>
    </div>
  </div>
  <div class="row">

  <div class="col-sm-4">
  <div class="well">
    <h2 class="text-center">Hashrate</h2>
    <h4 class="text-center">Hashrate 7 Days: <?php echo $SevenDayHash; ?></h4>
    <h4 class="text-center">Hashrate 1 Day: <?php echo $OneDayHash; ?></h4>
    <h4 class="text-center">Hashrate 1 Hour: <?php echo $OneHourHash; ?></h4>
    <h4 class="text-center">Hashrate 5 Minutes: <?php echo $FiveMinutesHash; ?></h4>
    <h4 class="text-center">Hashrate 1 Minute: <?php echo $OneMinuteHash; ?></h4>

    <h2 class="text-center">Hashrate2</h2>
    <h4 class="text-center">Hashrate 7 Days: <?php echo $SevenDayHash2; ?></h4>
    <h4 class="text-center">Hashrate 1 Day: <?php echo $OneDayHash2; ?></h4>
    <h4 class="text-center">Hashrate 1 Hour: <?php echo $OneHourHash2; ?></h4>
    <h4 class="text-center">Hashrate 5 Minutes: <?php echo $FiveMinutesHash2; ?></h4>
    <h4 class="text-center">Hashrate 1 Minute: <?php echo $OneMinuteHash2; ?></h4>

  </div>
  </div>

    <div class="col-sm-4">
    <div class="well">
        <h2 class="text-center">Shares and Diff</h2>
        <h4 class="text-center" <?php if ($CurrentDiff < $BestShare) {echo 'style="color:green"';} else {echo 'style="color:red"';}?>>Best Share: <?php echo $BestShare;  ?></h4>
        <h4 class="text-center">Current Diff: <?php echo $CurrentDiff;  ?></h4>
        <h2 class="text-center">Shares and Dif2f</h2>
        <h4 class="text-center" <?php if ($CurrentDiff < $BestShare2) {echo 'style="color:green"';} else {echo 'style="color:red"';}?>>Best Share: <?php echo $BestShare2;  ?></h4>
        <h4 class="text-center">Current Diff: <?php echo $CurrentDiff;  ?></h4>
    </div>
    </div>

      <div class="col-sm-4">
      <div class="well">
        <h2 class="text-center">Basic Stats</h2>
        <h4 class="text-center">Workers Online: <?php echo $WorkerAmount;  ?></h4>
        <h4 class="text-center">Btc Adress/ WorkerName : <?php echo $WorkerArray;  ?></h4>
        <h4 class="text-center">Workers Online: <?php echo $WorkerAmount2;  ?></h4>
        <h4 class="text-center">Btc Adress/ WorkerName : <?php echo $WorkerArray2;  ?></h4>
      </div>
      </div>

</div>
    </main>

  </body>
</html>
