  <main class="container-fluid">
    <div class="row">
          <div class="col-sm-12 well">
            <h1 class="text-center">Ads Stats</h1>
          </div>
          <div class="col-sm-6 well">
            <h2 class="text-center"> My Paying Ads: <?php echo $DailyIncomeMyPayingAds; ?>$</h2>
            <h2 class="text-center"> Total Value: <?php echo $TotalValueMyPayingAds; ?>$</h2>
            <h2 class="text-center"> Last Surfed: <?php echo $AdPackMyPayingAdsLastSurfed; ?></h2>
          </div>
          <div class="col-sm-6 well">
            <h2 class="text-center"> My Paying Crypto Ads: <?php echo $DailyIncomeMyPayingCryptoAds; ?> BTC</h2>
            <h2 class="text-center"> Total Value: <?php echo $TotalValueMyPayingCryptoAds; ?> BTC</h2>
            <h2 class="text-center"> Last Surfed: <?php echo $AdPackMyPayingCryptoAdsLastSurfed; ?></h2>
          </div>
    </div>
    <div class="row">
      <div class="col-sm-6">
        <div class="well">
          <h2 class="text-center">My Paying Ads</h2>
          <h3 class="text-center">AdPack1: <?php echo $ActiveMyPayingAds1; ?> of <?php echo $MaxAmountMyPayingAds1; ?></h3>
          <h3 class="text-center">AdPack2: <?php echo $ActiveMyPayingAds2; ?> of <?php echo $MaxAmountMyPayingAds2; ?></h3>
          <h3 class="text-center">AdPack3: <?php echo $ActiveMyPayingAds3; ?> of <?php echo $MaxAmountMyPayingAds3; ?></h3>
          <h3 class="text-center">AdPack4: <?php echo $ActiveMyPayingAds4; ?> of <?php echo $MaxAmountMyPayingAds4; ?></h3>
      </div>
    </div>
    <div class="col-sm-6">
      <div class="well">
          <form method="post" action="/?page=ads">
            <div class="form-group">
              <label for="mpaad1">MpaAd1</label>
              <input type="text" class="form-control" id="mpaad1" name="mpaad1">
            </div>
            <div class="form-group">
              <label for="mpaad2">MpaAd2:</label>
              <input type="text" class="form-control" id="mpaad2" name="mpaad2">
            </div>
            <div class="form-group">
              <label for="mpaad3">MpaAd3:</label>
              <input type="text" class="form-control" id="mpaad3" name="mpaad3">
            </div>
            <div class="form-group">
              <label for="mpaad4">MpaAd4:</label>
              <input type="text" class="form-control" id="mpaad4" name="mpaad4">
            </div>
            <button type="submit" class="btn btn-default" name="mpa">Submit</button>
          </form>
    </div>
  </div>

    </div>
    <div class="row">
      <div class="col-sm-6">
        <div class="well">
          <h2 class="text-center">My Paying Crypto Ads</h2>
          <h3 class="text-center">AdPack1: <?php echo $ActiveMypayingCryptoAds1; ?> of <?php echo $MaxAmountMypayingCryptoAds1; ?></h3>
          <h3 class="text-center">AdPack2: <?php echo $ActiveMypayingCryptoAds2; ?> of <?php echo $MaxAmountMypayingCryptoAds2; ?></h3>
          <h3 class="text-center">AdPack3: <?php echo $ActiveMypayingCryptoAds3; ?> of <?php echo $MaxAmountMypayingCryptoAds3; ?></h3>
          <h3 class="text-center">AdPack4: <?php echo $ActiveMypayingCryptoAds4; ?> of <?php echo $MaxAmountMypayingCryptoAds4; ?></h3>
      </div>
    </div>
    <div class="col-sm-6">
      <div class="well">
          <form method="post" action="/?page=ads">
            <div class="form-group">
              <label for="mpcaad1">MpCaAd1</label>
              <input type="text" class="form-control" id="mpcaad1" name="mpcaad1">
            </div>
            <div class="form-group">
              <label for="mpcaad2">MpCaAd2:</label>
              <input type="text" class="form-control" id="mpcaad2" name="mpcaad2">
            </div>
            <div class="form-group">
              <label for="mpcaad3">MpCaAd3:</label>
              <input type="text" class="form-control" id="mpcaad3" name="mpcaad3">
            </div>
            <div class="form-group">
              <label for="mpcaad4">MpCaAd4:</label>
              <input type="text" class="form-control" id="mpcaad4" name="mpcaad4">
            </div>
            <button type="submit" class="btn btn-default" name="mpca">Submit</button>
          </form>
    </div>
  </div>

    </div>
    <div class="row">
      <div class="col-sm-6">
        <div class="well">
          <h2 class="text-center">My Paying Ads</h2>
          <h3 class="text-center">Last Surfed: <?php echo $AdPackMyPayingAdsLastSurfed; ?> </h3>
          <h2 class="text-center">My Paying Crypto Ads</h2>
          <h3 class="text-center">Last Surfed: <?php echo $AdPackMyPayingCryptoAdsLastSurfed; ?> </h3>

      </div>
    </div>
    <div class="col-sm-6">
      <div class="well">
          <form class="form-inline" method="post" action="/?page=ads">
              <button type="submit" class="btn btn-default" name="mpas">Mpa Surfed</button>
            </form>
            <br>
            <form class="form-inline" method="post" action="/?page=ads">
              	<button type="submit" class="btn btn-default" name="mpcas">MpCa Surfed</button>
            	</form>
    </div>
  </div>

    </div>
  </main>
