<meta charset="utf-8">
<meta http-equiv="refresh" content="60">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<meta name="viewport" content="width=device-width">
<meta name="google" content="notranslate">
<script src="https://use.fontawesome.com/4c508f666e.js"></script>
<script src="https://use.typekit.net/ctv0mfx.js"></script>
<script>try{Typekit.load({ async: true });}catch(e){}</script>
<link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css" >
<link rel="stylesheet" href="css/main.css" >
<script src="bower_components/jquery/dist/jquery.min.js" charset="utf-8"></script>
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js" charset="utf-8"></script>
<title>Bitcoin Manager</title>
