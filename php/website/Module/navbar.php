
<!-- Navigation Grundgerüst -->
<nav  class="navbar navbar-inverse navbar-fixed-top">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
<!-- Website Name-->
			<a class="navbar-brand" href="/?page=home">Bitcoin Manager</a>
		</div>
<!-- Forsetzung Navigation -->
		<div id="navbar" class="collapse navbar-collapse">
			<ul class="nav navbar-nav">
        <li><a href="/">Home</a></li>
        <li><a href="/?page=ads">Ads</a></li>
				<li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="">Config
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="/?page=basic">Basic</a></li>
          <li><a href="/?page=adress">Btc Adressen</a></li>
          <li><a href="/?page=miner">Miner</a></li>
					<li><a href="/?page=genesis">Genesis</a></li>
					<li><a href="/?page=wanted">Wanted</a></li>
        </ul>
      </li>
			</ul>
<!-- Registrier- und Loginfunktion Navigation -->
			<ul class="nav navbar-nav navbar-right">
        <li ><a><strong>1</strong> <span class="glyphicon glyphicon-xbt"></span> <strong>=</strong></a></li>
        <li><a><strong ><?php echo  $EUR_BitcoinPrice;?></strong> <span class="glyphicon glyphicon-euro"></span> </a></li>
        <li><a><strong><?php echo $USD_BitcoinPrice; ?></strong> <span class="glyphicon glyphicon-usd"></span> </a></li>
        <li><a><strong><?php echo $CAD_BitcoinPrice; ?></strong> <span class="glyphicon glyphicon-usd"></span> </a></li>
				<li><a href="php/website/Module/logout.php" ><span class="glyphicon glyphicon-log-out"></span> Log out</a></li>
			</ul>
		</div>
	</div>
</nav>
