
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="well">
          <h3 class="text-center">Hashrate:
            <?php
            if($Hashrate < 1000){
              echo $Hashrate.' MH/S';
            }
            elseif ($Hashrate > 1000 || $HashrateGhs < 1000) {
              echo $HashrateGhs.' GH/S';
            }
            elseif ($HashrateGhs > 1000 || $HashrateThs < 1000) {
              echo $HashrateThs.' TH/S';
            }
            else {
              echo $HashratePhs.' PH/S';
            }
            ?>
          </h3>
          <h3 class="text-center"> Scrypt Hashrate:
            <?php
            echo $TotalMultipoolHashrate.' MH/S';
            ?>
          </h3>
          <h3 class="text-center"> Solo BTC Hashrate:
            <?php
            echo $OneMinuteHash;
            ?>
          </h3>
          <h3 class="text-center"> Genesis Hashrate:
            <?php
            echo $GenesisMiningHashpowerDisplay;
            ?>
          </h3>
          <h3 class="text-center"> Genesis Income per Day:
            <?php
            echo $GenesisMiningIncome - $GenesisMiningPowerCost.' $';
            ?>
          </h3>
          <h3 class="text-center"> Genesis Income per Month:
            <?php
            echo (($GenesisMiningIncome - $GenesisMiningPowerCost) * 30).' $';
            ?>
          </h3>
      </div>


    </div>
    </div>
  <div class="row">
  <div class="col-sm-4">
  <div class="well">
  <h1 class="text-center">Balances</h1>
  <table class="table">
  <thead>
  <tr>
  <th>Account</th>
  <th>BTC</th>
  <th>EURO</th>
  <th>USD</th>
  <th>CAD</th>
  </tr>
  </thead>
  <tbody>
    <tr <?php if( $ScriptPoolBalance <= 0.00001 ){echo $style; } ?> >
      <td>Multipool</td>
      <td><?php echo $ScriptPoolBalance; ?></td>
      <td><?php echo (round($ScriptPoolBalance * $EUR_BitcoinPrice,2)); ?></td>
      <td><?php echo (round($ScriptPoolBalance * $USD_BitcoinPrice,2));?></td>
      <td><?php echo (round($ScriptPoolBalance * $CAD_BitcoinPrice,2));?></td>
    </tr>
  <tr <?php if( $amount_Bitcoinde <= 0.00001 ){echo $style; } ?> >
    <td>Bitcoin.de</td>
    <td><?php echo $amount_Bitcoinde; ?></td>
    <td><?php echo (round($amount_Bitcoinde * $EUR_BitcoinPrice,2)); ?></td>
    <td><?php echo (round($amount_Bitcoinde * $USD_BitcoinPrice,2));?></td>
    <td><?php echo (round($amount_Bitcoinde * $CAD_BitcoinPrice,2));?></td>
  </tr>
  <tr <?php if( $IncomeBalance <= 0.00001 ){echo $style; } ?> >
    <td>Income</td>
    <td><?php echo $IncomeBalance; ?></td>
    <td><?php echo (round($IncomeBalance * $EUR_BitcoinPrice,2)); ?></td>
    <td><?php echo (round($IncomeBalance * $USD_BitcoinPrice,2));?></td>
    <td><?php echo (round($IncomeBalance * $CAD_BitcoinPrice,2));?></td>
  </tr>
  <tr <?php if( $SplitterBalance <= 0.00001 ){echo $style; } ?> >
    <td>Splitter</td>
    <td><?php echo $SplitterBalance; ?></td>
    <td><?php echo (round($SplitterBalance * $EUR_BitcoinPrice,2)); ?></td>
    <td><?php echo (round($SplitterBalance * $USD_BitcoinPrice,2));?></td>
    <td><?php echo (round($SplitterBalance * $CAD_BitcoinPrice,2));?></td>
  </tr>
<tr <?php if( $EnergyBalance <= 0.00001 ){echo $style; } ?> >
  <td>Energy</td>
  <td><?php echo $EnergyBalance; ?></td>
  <td><?php echo (round($EnergyBalance * $EUR_BitcoinPrice,2)); ?></td>
  <td><?php echo (round($EnergyBalance * $USD_BitcoinPrice,2));?></td>
  <td><?php echo (round($EnergyBalance * $CAD_BitcoinPrice,2));?></td>
</tr>
<tr <?php if( $InvestBalance <= 0.00001 ){echo $style; } ?> >
<td>Investment</td>
<td><?php echo $InvestBalance; ?></td>
<td><?php echo (round($InvestBalance * $EUR_BitcoinPrice,2)); ?></td>
<td><?php echo (round($InvestBalance * $USD_BitcoinPrice,2));?></td>
<td><?php echo (round($InvestBalance * $CAD_BitcoinPrice,2));?></td>
</tr>
<tr <?php if( $AdSplitBalance <= 0.00001 ){echo $style; } ?> >
<td>Ad Splitter</td>
<td><?php echo $AdSplitBalance; ?></td>
<td><?php echo (round($AdSplitBalance * $EUR_BitcoinPrice,2)); ?></td>
<td><?php echo (round($AdSplitBalance * $USD_BitcoinPrice,2));?></td>
<td><?php echo (round($AdSplitBalance * $CAD_BitcoinPrice,2));?></td>
</tr>
<tr <?php if( $MypayingadsBalance <= 0.00001 ){echo $style; } ?> >
<td>My Paying Ads</td>
<td><?php echo $MypayingadsBalance; ?></td>
<td><?php echo (round($MypayingadsBalance * $EUR_BitcoinPrice,2)); ?></td>
<td><?php echo (round($MypayingadsBalance * $USD_BitcoinPrice,2));?></td>
<td><?php echo (round($MypayingadsBalance * $CAD_BitcoinPrice,2));?></td>
</tr>
<tr <?php if( $MycryptoadsBalance <= 0.00001 ){echo $style; } ?> >
<td>My Paying Crypto Ads</td>
<td><?php echo $MycryptoadsBalance; ?></td>
<td><?php echo (round($MycryptoadsBalance * $EUR_BitcoinPrice,2)); ?></td>
<td><?php echo (round($MycryptoadsBalance * $USD_BitcoinPrice,2));?></td>
<td><?php echo (round($MycryptoadsBalance * $CAD_BitcoinPrice,2));?></td>
</tr>
  <tr>
  <td></td>
  <th>Total</th>
  <th>Total</th>
  <th>Total</th>
  <th>Total</th>
  </tr>
  <tr>
  <td></td>
  <?php $TotalTotalAmountBtc = $TotalWalletBalance  +  $amount_Bitcoinde + $ScriptPoolBalance; ?>
  <th <?php if( $TotalTotalAmountBtc <= 0.00001 ){echo $style; } ?>><?php echo (round($TotalTotalAmountBtc,8)); ?></th>
  <th <?php if( $TotalTotalAmountBtc <= 0.00001 ){echo $style; } ?>><?php echo (round($TotalTotalAmountBtc *  $EUR_BitcoinPrice,2));?>€</th>
  <th <?php if( $TotalTotalAmountBtc <= 0.00001 ){echo $style; } ?>><?php echo (round($TotalTotalAmountBtc *  $USD_BitcoinPrice,2));?>$</th>
  <th <?php if( $TotalTotalAmountBtc <= 0.00001 ){echo $style; } ?>><?php echo (round($TotalTotalAmountBtc *  $CAD_BitcoinPrice,2));?>$</th>
  </tr>
  </tbody>
  </table>
  </div>
  </div>

  <div class="col-sm-4">
  <div class="well">
  <h1 class="text-center">Stats</h1>

  <div class="row">
  <h2>Miners</h2>
  <table class="table">
    <thead>
    <tr>
    <th>Home</th>
    <th>BTC Hosted</th>
    <th>Scrypt Hosted</th>
    <th>Total</th>
    </tr>
    </thead>
    <tbody>
      <tr>
        <td><?php echo $HomeMiners + $ScryptHomeMiners; ?></td>
        <td><?php echo $RentingMinerAmount; ?></td>
        <td><?php echo $ScryptRentingMinerAmount; ?></td>
        <td><?php echo $TotalMiners + $ScryptMinerAmount; ?></td>
      </tr>
    </tbody>
  </table>
  </div>
  <div class="row">
  <h2>Needed Amounts</h2>
  <table class="table">
    <thead>
    <tr>
    <th>Name</th>
    <th>Amount</th>
    <th>Times</th>
    </tr>
    </thead>
    <tbody>
      <tr>
        <td>Energy Cost</td>
        <td><?php echo round($TotalEnergyCost - $EnergyBalanceCad,2); ?>$ (Cad)</td>
        <td><?php echo '0'; ?></td>
      </tr>
      <tr>
        <tr>
          <td>Genesis Miners</td>
          <td><?php echo round($GenesisMiningMinerCost - $InvestBalanceUsd,2); ?>$</td>
          <td><?php echo $WantedGenesisContract; ?></td>
        </tr>
        <tr>
      <tr>
        <td>Hosted Scrypt Miners</td>
        <td><?php echo round($HomeMinerInvestCost - $InvestBalanceCad,2); ?>$ (Cad)</td>
        <td><?php echo $HomeMinerWanted; ?></td>
      </tr>
      <tr>
        <td>Hosted Btc Miners</td>
        <td><?php echo round($RentingMinerInvestCost - $InvestBalanceCad,2); ?>$ (Cad)</td>
        <td><?php echo $RentMinerWanted; ?></td>
      </tr>
      <tr>
        <td>My Paying Ads</td>
        <td><?php echo $MaxValueMyPayingAds; ?>$</td>
        <td><?php echo $WantedMyPayingAds; ?></td>
      </tr>
      <tr>
        <td>My Paying Crypto Ads</td>
        <td><?php echo $MaxValueMyPayingCryptoAds; ?> BTC</td>
        <td><?php echo $WantedMyPayingCryptoAds; ?></td>
      </tr>
    </tbody>
  </table>
  </div>

  <div class="row">
  <h2>Btc Adresses</h2>
  <table class="table">
    <thead>
    <tr>
    <th>Account</th>
    <th>Adress</th>
    </tr>
    </thead>
    <tbody>
      <tr>
        <td>Income</td>
        <td><?php echo $IncomeAccountAdress; ?></td>
      </tr>
      <tr>
        <td>Splitter</td>
        <td><?php echo $SplittingAccountAdress; ?></td>
      </tr>
      <tr>
        <td>Invest</td>
        <td><?php echo $InvestAccountAdress; ?></td>
      </tr>
      <tr>
        <td>Energy</td>
        <td><?php echo $EnergyAccountAdress; ?></td>
      </tr>
      <tr>
        <td>FaucetBox</td>
        <td><?php echo $FaucetBoxAdress; ?></td>
      </tr>
      <tr>
        <td>Bitcoin.de</td>
        <td><?php echo $BitcoinDeAdress; ?></td>
      </tr>
      <tr>
        <td>My Paying Crypto Ads</td>
        <td><?php echo $MycryptoadsAdress; ?></td>
      </tr>
      <tr>
        <td>My Paying  Ads</td>
        <td><?php echo $MypayingadsAdress; ?></td>
      </tr>
      <tr>
        <td> Ads Splitter</td>
        <td><?php echo $AdsplitAdress; ?></td>
      </tr>
    </tbody>
  </table>
  </div>


  </div>
  </div>


  <div class="col-sm-4">
  <div class="well">
  <h1 class="text-center">Progress</h1>


  <h4 <?php if( $ProzentEnergy <= 0 || $RentingMinerAmount <= 0 ){echo $style; } ?>>Energy Cost</h4>
  <div class="progress" <?php if( $ProzentEnergy <= 0 || $RentingMinerAmount <= 0 ){echo $style; } ?>>
  <div class="progress-bar" role="progressbar" aria-valuenow="70"
    aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $ProzentEnergy ?>%">
    <strong><?php  if ($ProzentEnergyZeigen == True) { echo $MehrAls100ProzentMsg; } else {echo  $ProzentEnergy; }?> %</strong>
  </div>
  </div>

  <h4 <?php if( $ProzentGenesis <= 0 || $WantedGenesisContract == false){echo $style; } ?>>Genesis Miner</h4>
  <div class="progress" <?php if( $ProzentGenesis <= 0 || $WantedGenesisContract == false ){echo $style; } ?>>
  <div class="progress-bar" role="progressbar" aria-valuenow="70"
    aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $ProzentGenesis ?>%">
    <strong><?php  if ($ProzentGenesisZeigen == True) { echo $MehrAls100ProzentMsg; } else {echo  $ProzentGenesis; }?> %</strong>
  </div>
  </div>

  <h4 <?php if( $ProzentHomeMiner <= 0 || $HomeMinerWanted <= 0){echo $style; } ?>>Scrypt Miner</h4>
  <div class="progress" <?php if( $ProzentHomeMiner <= 0 || $HomeMinerWanted <= 0 ){echo $style; } ?>>
  <div class="progress-bar" role="progressbar" aria-valuenow="70"
    aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $ProzentHomeMiner ?>%">
    <strong><?php  if ($ProzentHomeMinerZeigen == True) { echo $MehrAls100ProzentMsg; } else {echo  $ProzentHomeMiner; }?> %</strong>
  </div>
  </div>

  <h4 <?php if($ProzentRentedMiner <= 0 || $RentMinerWanted <= 0 ){echo $style; } ?>>Btc Miner</h4>
  <div class="progress" <?php if( $ProzentRentedMiner <= 0 || $RentMinerWanted <= 0 ){echo $style; } ?>>
  <div class="progress-bar" role="progressbar" aria-valuenow="70"
    aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $ProzentRentedMiner ?>%">
    <strong><?php  if ($ProzentRentedMinerZeigen == True) { echo $MehrAls100ProzentMsg; } else {echo  $ProzentRentedMiner; }?> %</strong>
  </div>
  </div>

  <h4 <?php if( $ProzentMyPayingAds <= 0 ){echo $style; } ?>>My Paying Ads</h4>
  <div class="progress" <?php if( $ProzentMyPayingAds <= 0 ){echo $style; } ?>>
  <div class="progress-bar" role="progressbar" aria-valuenow="70"
    aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $ProzentMyPayingAds ?>%">
    <strong><?php echo $TotalActiveMyPayingAds.' of '.$MaxMyPayingAds.' Ad Packs'  ?></strong>
  </div>
  </div>

  <h4 <?php if( $ProzentMyPayingCryptoAds <= 0 ){echo $style; } ?>>My Paying Crypto Ads</h4>
  <div class="progress" <?php if( $ProzentMyPayingCryptoAds <= 0 ){echo $style; } ?>>
  <div class="progress-bar" role="progressbar" aria-valuenow="70"
    aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $ProzentMyPayingCryptoAds ?>%">
    <strong><?php echo $TotalActiveMyPayingCryptoAds.' of '.$MaxMyPayingCryptoAds.' Ad Packs'  ?></strong>
  </div>
  </div>

  <h4 <?php if( $ProzentBitcoinDe <= 0 ){echo $style; } ?>>Bitcoin.de Payout</h4>
  <div class="progress" <?php if( $ProzentBitcoinDe <= 0 ){echo $style; } ?>>
  <div class="progress-bar" role="progressbar" aria-valuenow="70"
    aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $ProzentBitcoinDe ?>%">
    <strong><?php  if ($ProzentBitcoinDeZeigen == True) { echo $MehrAls100ProzentMsg; } else {echo  $ProzentBitcoinDe; }?> %</strong>
  </div>
  </div>

   <h4 <?php if( $ProzentMultipool <= 0 ){echo $style; } ?>>Multipool</h4>
   <div class="progress" <?php if( $ProzentMultipool <= 0 ){echo $style; } ?>>
   <div class="progress-bar" role="progressbar" aria-valuenow="70"
     aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $ProzentMultipool ?>%">
     <strong><?php  if ($ProzentMultipoollZeigen == True) { echo date("H:i:s",$TimeLeft).' bis Payout'; } else {echo  $ProzentMultipool.'%'; }?></strong>
   </div>
   </div>


  <h4 <?php if( $ProzentIncome <= 0 ){echo $style; } ?>>Income Splitt</h4>
  <div class="progress" <?php if( $ProzentIncome <= 0 ){echo $style; } ?>>
  <div class="progress-bar" role="progressbar" aria-valuenow="70"
    aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $ProzentIncome ?>%">
    <strong><?php  if ($ProzentIncomeZeigen == True) { echo $MehrAls100ProzentMsg; } else {echo  $ProzentIncome; }?>%</strong>
  </div>
  </div>

  <h4 <?php if( $ProzentSplitter <= 0 ){echo $style; } ?>>Splitter Splitt</h4>
  <div class="progress" <?php if( $ProzentSplitter <= 0 ){echo $style; } ?>>
  <div class="progress-bar" role="progressbar" aria-valuenow="70"
    aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $ProzentSplitter ?>%">
    <strong><?php  if ($ProzentSplitterZeigen == True) { echo $MehrAls100ProzentMsg; } else {echo  $ProzentSplitter; }?> %</strong>
  </div>
  </div>

  <h4 <?php if( $ProzentAdSplitter <= 0 ){echo $style; } ?>>Ad Splitter</h4>
  <div class="progress" <?php if( $ProzentAdSplitter <= 0 ){echo $style; } ?>>
  <div class="progress-bar" role="progressbar" aria-valuenow="70"
    aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $ProzentAdSplitter ?>%">
    <strong><?php  if ($ProzentAdSplitterZeigen == True) { echo $MehrAls100ProzentMsg; } else {echo  $ProzentAdSplitter; }?> %</strong>
  </div>
  </div>


  </div>
  </div>




  </div>

  </div>
