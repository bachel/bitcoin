<?php
// Api Slushpool
$queryPoolData = file_get_contents('https://slushpool.com/accounts/profile/json/1232714-22a41baa1a996f942916f1255a43fb48');
$decodePoolData = json_decode($queryPoolData, true);
$TotalMiners = count($decodePoolData['workers']);
$SlushpoolBalance = $decodePoolData['confirmed_reward'];
$Hashrate = $decodePoolData['hashrate'];
$HashrateGhs = $Hashrate / 1000;
$HashrateThs = round($HashrateGhs / 1000,3);
$HashratePhs = round($HashrateThs / 1000,1);
$SlushpoolThreashold = $decodePoolData['send_threshold'];

// Api Multi Script Pool
$queryScryptPoolDataBalance = file_get_contents('https://prohashing.com/api/balances?key=e93fd73dd2ddaea93e5544481bc031e32d42236f46f57920335e8119c2451fa0');
$decodeScryptPoolDataBalance = json_decode($queryScryptPoolDataBalance,true);
$ScriptPoolBalance = round($decodeScryptPoolDataBalance[0]['balance'],8);

$queryScryptPoolDataMining = file_get_contents('https://prohashing.com/api/status?key=e93fd73dd2ddaea93e5544481bc031e32d42236f46f57920335e8119c2451fa0');
$decodeScryptPoolDataMining = json_decode($queryScryptPoolDataMining,true);
$ScryptHashrate = $decodeScryptPoolDataMining['acceptedHashrate'];
$MultipoolHashrate = $decodeScryptPoolDataMining['workers'][0]['acceptedHashrate'];
$MultipoolHashrate1 = $decodeScryptPoolDataMining['workers'][1]['acceptedHashrate'];
$TotalMultipoolHashrate = round(($MultipoolHashrate + $MultipoolHashrate1) / 1000000,2);
$ScryptMinerAmount = count($decodeScryptPoolDataMining['workers']);



 ?>
