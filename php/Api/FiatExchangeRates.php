<?php
// Exchange Queries
// Base EUR
$exch = curl_init();
curl_setopt($exch, CURLOPT_URL, 'http://api.fixer.io/latest');
curl_setopt($exch, CURLOPT_POST, false);
curl_setopt($exch, CURLOPT_RETURNTRANSFER, true);
// Base USD
$exch1 = curl_init();
curl_setopt($exch1, CURLOPT_URL, 'http://api.fixer.io/latest?base=USD');
curl_setopt($exch1, CURLOPT_POST, false);
curl_setopt($exch1, CURLOPT_RETURNTRANSFER, true);
// Base CAD
$exch2 = curl_init();
curl_setopt($exch2, CURLOPT_URL, 'http://api.fixer.io/latest?base=CAD');
curl_setopt($exch2, CURLOPT_POST, false);
curl_setopt($exch2, CURLOPT_RETURNTRANSFER, true);
// Get Exchange Data
// Base EUR
$exch_output = curl_exec ($exch);
curl_close ($exch);
$exch_output_json = json_decode($exch_output);
// Base USD
$exch_output1 = curl_exec ($exch1);
curl_close ($exch1);
$exch_output_json1 = json_decode($exch_output1);
// Base CAD
$exch_output2 = curl_exec ($exch2);
curl_close ($exch2);
$exch_output_json2 = json_decode($exch_output2);
// Set Exchange Variables
// Base EUR
$baseEUR_USD = $exch_output_json->rates->USD;
$baseEUR_CAD = $exch_output_json->rates->CAD;
// Base USD
$baseUSD_EUR = $exch_output_json1->rates->EUR;
$baseUSD_CAD = $exch_output_json1->rates->CAD;
// Base CAD
$baseCAD_EUR = $exch_output_json2->rates->EUR;
$baseCAD_USD = $exch_output_json2->rates->USD;
 ?>
