<?php
//Get Balances from Bitstamp
$BitstampBtcBalance = $BitstampConnection->balance()['btc_available'];
$BitstampEurBalance = $BitstampConnection->balance()['eur_available'];
// Get Lowest Ask Price
$BitstampLowestAskPrice = $BitstampConnection->ticker()['ask'];
$BitstampFee = $BitstampConnection->balance()['fee'];
// Calculate Amount of Btc
$BitstampBitcoinAmountBuyBeforeFee = round($BitstampEurBalance / $BitstampLowestAskPrice,8);
$BitstampBitcoinAmountBuyFee = round(($BitstampBitcoinAmountBuyBeforeFee * $BitstampFee) / 100,8);
$BitstampBitcoinAmountBuy = round($BitstampBitcoinAmountBuyBeforeFee - $BitstampBitcoinAmountBuyFee,8);

// Buy Bitcoin
if ($MaxBitstampBtcPrice < $BitstampLowestAskPrice && $BitstampEurBalance > 0) {
  $BitstampConnection->bitstamp_query("v2/buy/market/btceur/", array('amount'=>$BitstampBitcoinAmountBuy));
}
 //Withdraw Bitcoin
 if ($BitstampBtcBalance > 0) {
 $BitstampConnection->bitstamp_query("bitcoin_withdrawal/", array('amount'=>$BitstampBtcBalance,'address'=>$InvestAccountAdress,'instant'=>'0'));
 }





 ?>
