<?php

// Allgemeine Constants
// Set TimeZone and Get Time
date_default_timezone_set('Europe/Berlin');
$Zeit = date('H:i:s');
$Zeit2 = date('i');
$Zeit3 = date('H:i');
$PayoutTime = strtotime('05:00:00');
$TimeLeft = $PayoutTime -time();



//Website Constants
// Nachricht bei mehr als 100%
$MehrAls100ProzentMsg = 'Du bist über 100% sehr gut !!';
// Css Style falls etwas nicht angezeigt
$style = 'style="display:none;"';
//Progres Constants
$ProzentSlushpoolZeigen = False;
$ProzentBitcoinDeZeigen = False;
$ProzentIncomeZeigen = False;
$ProzentSplitterZeigen = False;
$ProzentFaucetBoxZeigen = False;
$ProzentHomeMinerZeigen = False;
$ProzentRentedMinerZeigen = False;
$ProzentEnergyZeigen = False;
$ProzentAdSplitterZeigen = False;
?>
