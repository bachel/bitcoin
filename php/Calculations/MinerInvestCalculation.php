<?php
$MinerCostCad = round(($MinerPrice * $baseUSD_CAD),2);
$ShippingCostCad = round(($ShippingCost * $baseUSD_CAD),2);
$ImportAmountCad = round( (($MinerCostCad + $ShippingCostCad)  * $ImportTaxCanada) /100 ,2);
$MinerCostInclImportCad = round(($MinerCostCad + $ShippingCostCad + $ImportAmountCad),2);

$RentingMinerInvestCost =  round(($RentMinerWanted * ($MinerCostInclImportCad + $RentingCostOverTimeframePerUnit)),2);


$ScryptMinerCostCad = round(($MinerPrice2 * $baseUSD_CAD),2);
$ScryptShippingCostCad = round(($ShippingCost2 * $baseUSD_CAD),2);
$ScryptImportAmountCad = round( (($ScryptMinerCostCad + $ScryptShippingCostCad)  * $ImportTaxCanada) /100 ,2);
$ScryptMinerCostInclImportCad = round(($ScryptMinerCostCad + $ScryptShippingCostCad + $ScryptImportAmountCad),2);
$HomeMinerInvestCost =  round(($HomeMinerWanted *  ($ScryptMinerCostInclImportCad + $ScryptRentingCostOverTimeframePerUnit)),2);

$WantedGenesisContract = false;
$GenesisMiningMinerCost = round($MinerPrice3,2);
$GenesisMiningHashpowerinGhs = $ApwPrice3 * 1000;
$GenesisMiningPowerCost = $GenesisMiningHashpowerinGhs * $MinerKw3;
$GenesisMiningHashpowerDisplayBase = $ApwPrice3;
$GenesisMiningHashpowerDisplay;
if ($GenesisMiningHashpowerDisplayBase >= 1000) {
    $GenesisMiningHashpowerDisplay = round(($GenesisMiningHashpowerDisplayBase/1000),2).' Ph/s';
}
else {
  $GenesisMiningHashpowerDisplay = $GenesisMiningHashpowerDisplayBase.' Th/s';
}
$GenesisHashesForCalculator = bcmul($ApwPrice3,'1000000000000');
$GenesisinProfit = file_get_contents('https://alloscomp.com/bitcoin/calculator/json?hashrate='.$GenesisHashesForCalculator);
$decodeGenesisProfit = json_decode($GenesisinProfit,true);
$GenesisMiningIncome = round(($decodeGenesisProfit['dollars_per_hour'] * 24),2);
if ($GenesisMiningIncome < $GenesisMiningPowerCost) {
  $WantedGenesisContract = false;
}


 ?>
