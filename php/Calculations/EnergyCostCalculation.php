<?php
// SHA256
$RentingUnitMonthlyCost = round(($KwPrice + $ApwRentPrice) * $MinerKw,2);
$RentingCostOverTimeframePerUnit = round($RentingUnitMonthlyCost * $RentingTimeFrame,2);
$RentingMinerAmount = $TotalMiners - $HomeMiners - $MinerOnSale;
$RentingEnergyCost = $RentingMinerAmount * $RentingCostOverTimeframePerUnit;
// Scrypt
$ScryptRentingUnitMonthlyCost = round(($KwPrice + $ApwRentPrice) * $MinerKw2,2);
$ScryptRentingCostOverTimeframePerUnit = round($ScryptRentingUnitMonthlyCost * $RentingTimeFrame,2);
$ScryptRentingMinerAmount = $ScryptMinerAmount - $ScryptHomeMiners - $ScryptMinerOnSale;
$ScryptRentingEnergyCost = $ScryptRentingMinerAmount * $ScryptRentingCostOverTimeframePerUnit;

$TotalEnergyCost = round($RentingEnergyCost + $ScryptRentingEnergyCost)

 ?>
