<?php
//My Paying  Ads
$sqlMypayingAds1 = 'SELECT * FROM MyPayingAds WHERE id = 1';
$queryMypayingAds1  = mysqli_query($mysqli,$sqlMypayingAds1 ,$resultmode);
$getMypayingAds1  = mysqli_fetch_assoc($queryMypayingAds1 );
$AdPackMyPayingAds1 = $getMypayingAds1['AdPack'];
$ValueMyPayingAds1 = $getMypayingAds1['Value'];
$MaxAmountMyPayingAds1 = $getMypayingAds1['MaxAmount'];
$ActiveMyPayingAds1 = $getMypayingAds1['Active'];

$sqlMypayingAds2 = 'SELECT * FROM MyPayingAds WHERE id = 2';
$queryMypayingAds2  = mysqli_query($mysqli,$sqlMypayingAds2 ,$resultmode);
$getMypayingAds2  = mysqli_fetch_assoc($queryMypayingAds2 );
$AdPackMyPayingAds2 = $getMypayingAds2['AdPack'];
$ValueMyPayingAds2 = $getMypayingAds2['Value'];
$MaxAmountMyPayingAds2 = $getMypayingAds2['MaxAmount'];
$ActiveMyPayingAds2 = $getMypayingAds2['Active'];

$sqlMypayingAds3 = 'SELECT * FROM MyPayingAds WHERE id = 3';
$queryMypayingAds3  = mysqli_query($mysqli,$sqlMypayingAds3 ,$resultmode);
$getMypayingAds3  = mysqli_fetch_assoc($queryMypayingAds3 );
$AdPackMyPayingAds3 = $getMypayingAds3['AdPack'];
$ValueMyPayingAds3 = $getMypayingAds3['Value'];
$MaxAmountMyPayingAds3 = $getMypayingAds3['MaxAmount'];
$ActiveMyPayingAds3 = $getMypayingAds3['Active'];

$sqlMypayingAds4 = 'SELECT * FROM MyPayingAds WHERE id = 4';
$queryMypayingAds4  = mysqli_query($mysqli,$sqlMypayingAds4 ,$resultmode);
$getMypayingAds4  = mysqli_fetch_assoc($queryMypayingAds4 );
$AdPackMyPayingAds4 = $getMypayingAds4['AdPack'];
$ValueMyPayingAds4 = $getMypayingAds4['Value'];
$MaxAmountMyPayingAds4 = $getMypayingAds4['MaxAmount'];
$ActiveMyPayingAds4 = $getMypayingAds4['Active'];

//My Paying  Crypto Ads
$sqlMypayingCryptoAds1 = 'SELECT * FROM MyPayingAds WHERE id = 5';
$queryMypayingCryptoAds1  = mysqli_query($mysqli,$sqlMypayingCryptoAds1 ,$resultmode);
$getMypayingCryptoAds1  = mysqli_fetch_assoc($queryMypayingCryptoAds1 );
$AdPackMypayingCryptoAds1 = $getMypayingCryptoAds1['AdPack'];
$ValueMypayingCryptoAds1 = $getMypayingCryptoAds1['Value'];
$MaxAmountMypayingCryptoAds1 = $getMypayingCryptoAds1['MaxAmount'];
$ActiveMypayingCryptoAds1 = $getMypayingCryptoAds1['Active'];

$sqlMypayingCryptoAds2 = 'SELECT * FROM MyPayingAds WHERE id = 6';
$queryMypayingCryptoAds2  = mysqli_query($mysqli,$sqlMypayingCryptoAds2 ,$resultmode);
$getMypayingCryptoAds2  = mysqli_fetch_assoc($queryMypayingCryptoAds2 );
$AdPackMypayingCryptoAds2 = $getMypayingCryptoAds2['AdPack'];
$ValueMypayingCryptoAds2 = $getMypayingCryptoAds2['Value'];
$MaxAmountMypayingCryptoAds2 = $getMypayingCryptoAds2['MaxAmount'];
$ActiveMypayingCryptoAds2 = $getMypayingCryptoAds2['Active'];

$sqlMypayingCryptoAds3 = 'SELECT * FROM MyPayingAds WHERE id = 7';
$queryMypayingCryptoAds3  = mysqli_query($mysqli,$sqlMypayingCryptoAds3 ,$resultmode);
$getMypayingCryptoAds3  = mysqli_fetch_assoc($queryMypayingCryptoAds3 );
$AdPackMypayingCryptoAds3 = $getMypayingCryptoAds3['AdPack'];
$ValueMypayingCryptoAds3 = $getMypayingCryptoAds3['Value'];
$MaxAmountMypayingCryptoAds3 = $getMypayingCryptoAds3['MaxAmount'];
$ActiveMypayingCryptoAds3 = $getMypayingCryptoAds3['Active'];

$sqlMypayingCryptoAds4 = 'SELECT * FROM MyPayingAds WHERE id = 8';
$queryMypayingCryptoAds4  = mysqli_query($mysqli,$sqlMypayingCryptoAds4 ,$resultmode);
$getMypayingCryptoAds4  = mysqli_fetch_assoc($queryMypayingCryptoAds4 );
$AdPackMypayingCryptoAds4 = $getMypayingCryptoAds4['AdPack'];
$ValueMypayingCryptoAds4 = $getMypayingCryptoAds4['Value'];
$MaxAmountMypayingCryptoAds4 = $getMypayingCryptoAds4['MaxAmount'];
$ActiveMypayingCryptoAds4 = $getMypayingCryptoAds4['Active'];

// Last Surfed
$sqlMypayingAdsLast = 'SELECT * FROM MyPayingAdsSurfed WHERE id = 1';
$queryMypayingAdsLast   = mysqli_query($mysqli,$sqlMypayingAdsLast ,$resultmode);
$getMypayingAdsLast  = mysqli_fetch_assoc($queryMypayingAdsLast );
$AdPackMyPayingAdsLastSurfed = $getMypayingAdsLast['mpa'];
$AdPackMyPayingCryptoAdsLastSurfed = $getMypayingAdsLast['mpca'];

// Adfree + MaxAmount
$sqlAdFree = 'SELECT * FROM MyPayingAdsSurfFree WHERE id = 2';
$queryAdFree = mysqli_query($mysqli,$sqlAdFree,$resultmode);
$getAdFree = mysqli_fetch_assoc($queryAdFree);
$AdFreePrice = $getAdFree['Price'];
$AdFreePurchased = $getAdFree['Purchased'];
$AdLimitAmount = $getAdFree['Amount'];
$MemberAmount = $getAdFree['Member'];

$sqlAdFree2 = 'SELECT * FROM MyPayingAdsSurfFree WHERE id = 3';
$queryAdFree2 = mysqli_query($mysqli,$sqlAdFree2,$resultmode);
$getAdFree2 = mysqli_fetch_assoc($queryAdFree2);
$AdFreePrice2 = $getAdFree2['Price'];
$AdFreePurchased2 = $getAdFree2['Purchased'];
$AdLimitAmount2 = $getAdFree2['Amount'];
$MemberAmount2 = $getAdFree2['Member'];

 ?>
